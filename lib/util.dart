import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class Util {
  static FutureBuilder loadTextFromFile(BuildContext context, String fileName, TextAlign align) {
    var futureBuilder = new FutureBuilder(
        future: getFileData(fileName),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
            case ConnectionState.waiting:
              return new Text('loading...');
            default:
              if (snapshot.hasError)
                return new Text('Error: ${snapshot.error}');
              else
                return new Text(
                  snapshot.data,
                  textAlign: align,
                );
          }
        });
    return futureBuilder;
  }

  static Future<String> getFileData(String path) async {
    return await rootBundle.loadString(path);
  }
}
