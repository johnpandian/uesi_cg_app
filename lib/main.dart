import 'package:uesi_cg_app/home.dart';
import 'package:uesi_cg_app/constants.dart';
import 'package:flutter/material.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //Constants.loadConstants();
    return new MaterialApp(
      debugShowCheckedModeBanner : false,
      title: 'Career Guidance by UESI',
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: new HomeScreen(),
      
    );
  }
}

