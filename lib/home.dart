import 'package:flutter/material.dart';
import 'package:uesi_cg_app/drawer.dart';
import 'package:uesi_cg_app/constants.dart';
import 'package:uesi_cg_app/pages/article-view.dart';
import 'package:uesi_cg_app/pages/streams-list.dart';

class HomeScreen extends StatefulWidget {

  bool isExpanded = false;
  bool isExpanded1 = false;
  bool isExpanded2 = false;

  @override
  _HomeScreenState createState() {
    return new _HomeScreenState();
  }
}

class _HomeScreenState extends State<HomeScreen>
    with TickerProviderStateMixin<HomeScreen> {
  Widget build(BuildContext context) {
    /*int test = Constants.test;
    print("JP text HM : $test");
    Constants.test++;*/
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Career Guidance'),
      ),
      drawer: new NavigationDrawerDemo(),
      body: getHomeScreenItems(context),
    );
  }

  Widget getHomeItems(BuildContext context) {
    return new ListView(
      children: <Widget>[
        new FittedBox(
          child: new Image.asset(
            'assets/home.jpg',
          ),
        ),
        new Container(
          padding: EdgeInsets.all(15.0),
          child: Constants.getHomePageIntroText(context),
        ),
        GridView.count(
          crossAxisCount: 2,
          primary: false,
          scrollDirection: Axis.vertical,
          crossAxisSpacing: 10.0,
          padding: EdgeInsets.all(10.0),
          children: <Widget>[
            Text("Hi"),
            Text("Hi"),
            Text("Hi"),
            Text("Hi"),
          ],
        ),
        new Center(
          child: new Text(
            "Supported by NEGF Career Guidance Cell",
            overflow: TextOverflow.ellipsis,
            style: new TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        new Text(" "),
      ],
    );
  }

  onSelectItem(String courseType)
  {
    Navigator.push(
      context,
      new MaterialPageRoute(
        builder: (context) => new StreamsList(
          courseType: courseType,
        ),
      ),
    );
  }

  openArticle(int no, String title)
  {
    Navigator.push(
      context,
      new MaterialPageRoute(
        builder: (context) => new ArticleView(
          articleNo: no,
          articleTitle: title,
        ),
      ),
    );
  }

  Widget getHomeScreenItems(BuildContext context) {

    bool isExpanded1 = false;
    bool isExpanded2 = false;

    List<String> coursesList = new List<String>();
    coursesList.add("After 10th Courses");
    coursesList.add("Undergraduate Courses");
    coursesList.add("Diploma Courses");
    coursesList.add("Postgraduate Courses");
    coursesList.add("Ph.D Courses");
    coursesList.add("Certification Courses");

    TextStyle paragraphStyle = TextStyle(
      color: Colors.black,
      fontSize: 15.0,
    );

    TextStyle headerStyle = TextStyle(
      color: Colors.black,
      fontSize: 18.0,
      fontWeight: FontWeight.bold,
    );

    return new ListView(
      children: <Widget>[
        new Container(
          child: new RichText(
            text: TextSpan(
              text: "Courses",
              style: headerStyle,
            ),
            textAlign: TextAlign.center,
          ),
          padding: EdgeInsets.only(top : 15.0),
        ),
        new Container(
          child: GridView.count(
            crossAxisCount: 3,
            primary: false,
            scrollDirection: Axis.vertical,
            crossAxisSpacing: 10.0,
            padding: EdgeInsets.all(5.0),
            children: coursesList
                .map((name) =>
                  Card(
                    child: new InkWell(
                      child: new Column(
                        children: <Widget>[
                          new Container(
                            padding: EdgeInsets.all(10.0),
                            child: new Center(
                              child: Image.asset(
                                "assets/icons/" + Constants.getIconName(name),
                                width: 50.0,
                                height: 50.0,
                              ),
                            ),
                          ),
                          new Container(
                            padding: EdgeInsets.all(5.0),
                            child: Center(
                                child: Text(
                                  name,
                                  textAlign: TextAlign.center,
                                )),
                          )
                        ],
                      ),
                      onTap: () => onSelectItem(name),
                    ),
                  ),)
                .toList(),
            shrinkWrap: true,
          ),
        ),
        new Container(
          child: Divider(),
        ),
        new Container(
          padding: EdgeInsets.only(
            top: 5.0,
            bottom: 5.0
          ),
          child: new RichText(
            text: TextSpan(
              text: "Lastest Articles",
              style: headerStyle,
            ),
            textAlign: TextAlign.center,
          ),
        ),
        new Container(
          child: Card(
            child: ListTile(
              isThreeLine: true,
              title: Text("Biblical Basis of Work"),
              subtitle: Text(
                Constants.getArticleDescription(3),
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
              ),
              leading: new Image(
                image: new AssetImage('assets/icons/' + Constants.getIconName("Biblical Basis of Work")),
                height: 60.0,
                width: 60.0,
              ),
              onTap: () => openArticle(0, "Biblical Basis of Work"),
            ),
          ),
        ),
        new Container(
          child: Card(
            child: ListTile(
              isThreeLine: true,
              title: Text("Tent Making"),
              subtitle: Text(
                Constants.getArticleDescription(4),
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
              ),
              leading: new Image(
                image: new AssetImage('assets/icons/' + Constants.getIconName("Tent Making")),
                height: 60.0,
                width: 60.0,
              ),
              onTap: () => openArticle(4, "Tent Making"),
            ),
          ),
        ),
        new Container(
          child: new Container(
            child: new Column(
              children: <Widget>[
                RichText(
                  text: TextSpan(
                    text: "Greetings from the NEGF chairperson!",
                    style: headerStyle,
                  ),
                  textAlign: TextAlign.left,
                ),
                new Container(
                  child: new Column(
                    children: <Widget>[
                      new AnimatedSize(
                        duration: const Duration(milliseconds: 500),
                        vsync: this,
                        child: RichText(
                          maxLines: widget.isExpanded1 ? 100 : 5,
                          overflow: TextOverflow.ellipsis,
                          textAlign: TextAlign.justify,
                          text: TextSpan(
                            text: "To tick the right one among multiple options is difficult in deciding anything that has lasting impact on our lives.  Those who are God centered will go by Godly principles rather than what the world popularizes. Career Guidance Cell of NEGF has developed an App that is apt for such decision makers who wonder which path would be the best. It brings to you a collection of varied articles on topics relevant to anyone who wants to know Godly principles and available avenues. It also throws light on tent making apart from giving information on scholarships. The CG Cell is aiming at providing linkage to job opportunities also in future.",
                            style: paragraphStyle,
                            children: [
                              new TextSpan(
                                text: "\n\nI request you to share this App with those who are in need of such assistance within UESI fraternity. Please give your valuable feedback to NEGF CG cell for further improvement.  In all these may the Name of our Lord be exalted.",
                                style: paragraphStyle,
                              ),
                              new TextSpan(
                                text: "\n\nMr. Selwyn Samraj, NEGF chairperson",
                                style: Constants.italicParagraphStyle,
                              )
                            ],
                          ),
                        ),
                      ),
                      new Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          GestureDetector(
                            //textColor: Colors.blue,
                            child: new Container(
                              padding: EdgeInsets.only(top: 5.0),
                              child: RichText(
                                text: TextSpan(
                                  text: widget.isExpanded1 ? "Read less.." : "Read more...",
                                  style: Constants.paragraphStyleBlue,
                                ),
                              ),
                            ),
                            onTap: () => setState(() {
                              widget.isExpanded1 = !widget.isExpanded1;
                            }),
                          ),
                        ],
                      ),
                    ],
                  ),
                  padding: EdgeInsets.only(top: 10.0, bottom: 0.0),
                ),

              ],
            ),
            padding: EdgeInsets.only(
              top: 20.0,
              left: 10.0,
              right: 10.0,
              bottom: 10.0,
            ),
          ),
        ),
        new Container(
          child: new Container(
            child: new Column(
              children: <Widget>[
                RichText(
                  text: TextSpan(
                    text: "Greetings from a Civil Servant!",
                    style: headerStyle,
                  ),
                  textAlign: TextAlign.left,
                ),
                new Container(
                  child: new Column(
                    children: <Widget>[
                      new AnimatedSize(
                        duration: const Duration(milliseconds: 500),
                        vsync: this,
                        child: RichText(
                          maxLines: widget.isExpanded2 ? 100 : 5,
                          overflow: TextOverflow.ellipsis,
                          textAlign: TextAlign.justify,
                          text: TextSpan(
                            text: "Till I joined the Customs Department clearing the Staff Selection Exam, I did not know there is something called Civil Service Exam to join straight as Class I employee. So, I started encouraging the students to appear for this exam. Some of them successfully cleared and occupied important positions. Now, we get opportunities to expose the students through Career Guidance Cell to various courses available and the job openings in different field.",
                            style: paragraphStyle,
                            children: [
                              new TextSpan(
                                text: "\n\n I am excited to know that CGC is taking another step forward by introducing an app to encourage students to have great exposure. This is the need of the hour that the children of God occupy important position of decision making and delivering things with integrity in India like Joseph, Daniel, Esther and other characters in Scripture. I wish all the best to CGC!",
                                style: paragraphStyle,
                              ),
                              new TextSpan(
                                text: "\n\nMr. J.S.A. Julius, IRS, (Retd)",
                                style: Constants.italicParagraphStyle,
                              ),
                            ],
                          ),
                        ),
                      ),
                      new Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          new GestureDetector(
                            //textColor: Colors.blue,
                            child: new Container(
                              padding: EdgeInsets.only(top: 5.0),
                              child: RichText(
                                  text: TextSpan(
                                  text: widget.isExpanded2 ? "Read less.." : "Read more...",
                                  style: Constants.paragraphStyleBlue,
                                ),
                              ),
                            ),
                            onTap: () => setState(() {
                              widget.isExpanded2 = !widget.isExpanded2;
                            }),
                          ),
                        ],
                      ),
                    ],
                  ),
                  padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
                ),

              ],
            ),
            padding: EdgeInsets.only(
              top: 0.0,
              left: 10.0,
              right: 10.0,
              bottom: 0.0,
            ),
          ),
        ),
        new Container(
          child: Divider(),
        ),
        new Container(
          padding: EdgeInsets.only(bottom:20.0),
          child: RichText(
            text: TextSpan(
              text: "Supported by NEGF Carreer Guidance Cell",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.black,
              ),
            ),
            textAlign: TextAlign.center,
          ),
        ),
      ],
    );
  }
}
