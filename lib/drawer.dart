import 'package:flutter/material.dart';
import 'package:uesi_cg_app/home.dart';
import 'package:uesi_cg_app/pages/about.dart';
import 'package:uesi_cg_app/pages/article-view.dart';
import 'package:uesi_cg_app/pages/articles-list.dart';
import 'package:uesi_cg_app/pages/avenues-trends.dart';
import 'package:uesi_cg_app/pages/biblical-basis.dart';
import 'package:uesi_cg_app/pages/choose-career.dart';
import 'package:uesi_cg_app/pages/course-types.dart';
import 'package:uesi_cg_app/pages/exam-types.dart';
import 'package:uesi_cg_app/pages/scholarships-list.dart';
import 'package:uesi_cg_app/pages/streams-list.dart';
import 'package:uesi_cg_app/pages/study-techniques.dart';
import 'page.dart';

class DrawerItem {
  String title;
  //IconData icon;
  String fileName;

  DrawerItem(this.title, this.fileName);
}

class NavigationDrawerDemo extends StatefulWidget {

  //static NavigationDrawerDemo navBar = null;
  //static NavigationDrawerState navState = null;

  @override
  State<StatefulWidget> createState() {
    return new NavigationDrawerState();
  }
}

class NavigationDrawerState extends State<NavigationDrawerDemo> {
  int _selectedIndex = 0;
  //print("set to 0");

  final drawerItems = [
    new DrawerItem("Home", "assets/res/home.txt"),
    new DrawerItem("Articles", "assets/content/choose-career.txt"),
    new DrawerItem("Courses", ""),
    new DrawerItem("Entrance Exams", ""),
    new DrawerItem("Scholarships", ""),
    new DrawerItem("About", "")
  ];

  _getDrawerItemScreen(int pos) {
    if(pos == 0) {
      return new HomeScreen();
    }
    else if(pos == 1) {
      return new ArticlesList(drawerItem: drawerItems[_selectedIndex]);
    }
    else if(pos == 2) {
        return new CourseTypes(drawerItem: drawerItems[_selectedIndex]);
    }
    else if(pos == 3) {
      return new ExamTypes(drawerItem: drawerItems[_selectedIndex]);
    }
    else if(pos == 4) {
      return new ScholarshipsList(drawerItem: drawerItems[_selectedIndex]);
    }
    else if(pos == 5) {
      return new AboutPage(drawerItem: drawerItems[_selectedIndex]);
    }
    else {
      return new HomeScreen();
    }
  }

  _onSelectItem(int index) {
    setState(() {
      _selectedIndex = index;
      _getDrawerItemScreen(_selectedIndex);
    });
    Navigator.of(context).pop(); // close the drawer
    Navigator.push(
      context,
      new MaterialPageRoute(
        builder: (context) => _getDrawerItemScreen(_selectedIndex),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> drawerOptions = [];
    for (var i = 0; i < drawerItems.length; i++) {
      var d = drawerItems[i];
      /*drawerOptions.add(new ListTile(
        leading: new Icon(d.icon),
        title: new Text(
          d.title,
          style: new TextStyle(fontSize: 14.0, fontWeight: FontWeight.w400),
        ),
        selected: i == _selectedIndex,
        onTap: () => _onSelectItem(i),
      ));*/
      drawerOptions.add(
        new ListTile(
            title: new Text(
              d.title,
              style: new TextStyle(fontSize: 14.0, fontWeight: FontWeight.w400),
            ),
            //selected: i == _selectedIndex,
            onTap: () => _onSelectItem(i),
        ),
        /*new ExpansionTile(
          title: new Text(
            d.title,
            style: new TextStyle(fontSize: 14.0, fontWeight: FontWeight.w400),
          ),
          children: <Widget>[
            new ListTileTheme(
              child: new ListTile(
                title: new Text('Sub 1',
                  style: new TextStyle(fontSize: 12.0, fontWeight: FontWeight.w400),
                ),
                contentPadding: EdgeInsets.only(left:30.0,top: 0.0, bottom: 0.0),
                selected: i == _selectedIndex,
                onTap: () => _onSelectItem(i),
              ),
            ),
            new ListTileTheme(
              child: new ListTile(
                title: new Text('Sub 2',
                  style: new TextStyle(fontSize: 12.0, fontWeight: FontWeight.w400),
                ),
                contentPadding: EdgeInsets.only(left:30.0,top: 0.0, bottom: 0.0),
              ),
            ),
          ],
        ),*/
      );
    }

    return new Drawer(
      child: new Column(
        children: <Widget>[
          new UserAccountsDrawerHeader(
            accountName: new Text(
              "Career Guidance",
              style: new TextStyle(fontSize: 18.0, fontWeight: FontWeight.w500),
            ),
            accountEmail: new Text(
              "By UESI NEGF CG Cell",
              style: new TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
            ),
          ),
          new Column(children: drawerOptions)
        ],
      ),
    );
  }
}
