import 'package:flutter/material.dart';
import 'package:uesi_cg_app/markdown/flutter_markdown.dart';
import 'package:meta/meta.dart';
import 'package:uesi_cg_app/constants.dart';

import 'package:uesi_cg_app/drawer.dart';
import 'package:url_launcher/url_launcher.dart';

class ChooseCareer extends StatefulWidget {
  final DrawerItem drawerItem;

  ChooseCareer({Key key, @required this.drawerItem}) : super(key: key);

  @override
  ChooseCareerState createState() {
    return new ChooseCareerState();
  }
}

class ChooseCareerState extends State<ChooseCareer> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(
          widget.drawerItem.title,
        ),
      ),
      drawer: new NavigationDrawerDemo(),
      body: Markdown(
          data : Constants.chooseCareerText,
          onTapLink: (href) {launch(href);}
      ),
    );
  }
}
