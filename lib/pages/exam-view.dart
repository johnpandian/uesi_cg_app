import 'package:flutter/material.dart';
import 'package:uesi_cg_app/markdown/flutter_markdown.dart';
import 'package:meta/meta.dart';
import 'package:uesi_cg_app/constants.dart';

import 'package:uesi_cg_app/drawer.dart';
import 'package:url_launcher/url_launcher.dart';

class ExamView extends StatefulWidget {
  final Map<String,dynamic> exam;
  final String examName;

  ExamView({Key key, this.exam, this.examName}) : super(key: key);

  @override
  ExamViewState createState() {
    return new ExamViewState();
  }
}

class ExamViewState extends State<ExamView> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(
          widget.examName,
        ),
      ),
      drawer: new NavigationDrawerDemo(),
      body: getExamView(),
    );
  }

  Widget getExamView()
  {
    print(widget.exam);
    List<Widget> listView = new List<Widget>();

    listView.add(
      Container(
        padding: EdgeInsets.all(20.0),
        child: RichText(
            text: TextSpan(
              text: widget.exam['fname'],
              style: Constants.headerStyle,
            )
        ),
      ),
    );

    listView.add(
      Container(
        padding: EdgeInsets.only(
          left: 20.0,
          right: 20.0,
        ),
        child: RichText(
            textAlign: TextAlign.justify,
            text: TextSpan(
              text: widget.exam['desc'],
              style: Constants.paragraphStyle,
            )
        ),
      ),
    );

    listView.add(
      new Container(
        padding: EdgeInsets.all(10.0),
        child: new Center(
          child: new FlatButton(
            color: Colors.blue,
            textColor: Colors.white,
            child: Text("View more details"),
            onPressed: () => launch("https://collegedunia.com/exams/" + widget.exam['link']),
          ),
        ),
      ),
    );

    return ListView(
      children: listView,
    );
  }

  Widget getCourseView()
  {
    print(widget.exam);

    Map<String, dynamic> courseDetails = widget.exam['details'];

    List<Widget> listView = new List<Widget>();

    listView.add(
      Container(
        padding: EdgeInsets.all(20.0),
        child: RichText(
            text: TextSpan(
              text: widget.examName,
              style: Constants.headerStyle,
            )
        ),
      ),
    );

    listView.add(
      Container(
        padding: EdgeInsets.only(
          left: 20.0,
          right: 20.0,
        ),
        child: RichText(
          textAlign: TextAlign.justify,
          text: TextSpan(
            text: courseDetails['about'],
            style: Constants.paragraphStyle,
          )
        ),
      ),
    );
    
    if(courseDetails.containsKey("details")){
      listView.add(
        Container(
          padding: EdgeInsets.only(
            left: 20.0,
            right: 20.0,
            top: 20.0,
          ),
          child: RichText(
            textAlign: TextAlign.center,
              text: TextSpan(
                text: "Course Details",
                style: Constants.headerStyle,
              )
          ),
        ),
      );

      List<dynamic> details = courseDetails['details'];
      for(Map<String,dynamic> detail in details) {
        listView.addAll(getCourseDetails(detail['key'], detail['value']));
      }
    }

    if(courseDetails.containsKey("jobs")){
      listView.add(
        Container(
          padding: EdgeInsets.only(
            left: 20.0,
            right: 20.0,
            top: 20.0,
          ),
          child: RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: "Job Profiles",
                style: Constants.headerStyle,
              )
          ),
        ),
      );

      List<dynamic> jobs = courseDetails['jobs'];
      for(Map<String,dynamic> job in jobs) {
        listView.addAll(getJobDetails(job['title'], job['description'], job['salary']));
      }
    }

    listView.add(
      Container(
        padding: EdgeInsets.all(20.0),
        child: Text(""),
      ),
    );

    return ListView(
      children: listView,
    );
  }

  List<Widget> getCourseDetails(String key, String value) {
    List<Widget> detailsList = new List();
    detailsList.add(
      new Container(
        padding: EdgeInsets.only(
          left: 20.0,
          right: 20.0,
          top: 10.0,
        ),
        child: RichText(
          text: TextSpan(
            text: key,
            style: Constants.boldStyle,
          ),
        ),
      )
    );
    detailsList.add(
        new Container(
          padding: EdgeInsets.only(
            left: 20.0,
            right: 20.0,
            top: 5.0,
          ),
          child: RichText(
            text: TextSpan(
              text: value,
              style: Constants.paragraphStyle,
            ),
          ),
        )
    );
    return detailsList;
  }

  List<Widget> getJobDetails(String title, String description, String salary) {
    List<Widget> detailsList = new List();
    detailsList.add(
        new Container(
          padding: EdgeInsets.only(
            left: 20.0,
            right: 20.0,
            top: 10.0,
          ),
          child: RichText(
            text: TextSpan(
              text: title,
              style: Constants.boldStyle,
            ),
          ),
        )
    );
    detailsList.add(
        new Container(
          padding: EdgeInsets.only(
            left: 20.0,
            right: 20.0,
            top: 5.0,
          ),
          child: RichText(
            text: TextSpan(
              text: description,
              style: Constants.paragraphStyle,
            ),
          ),
        )
    );

    detailsList.add(
        new Container(
          padding: EdgeInsets.only(
            left: 20.0,
            right: 20.0,
            top: 5.0,
          ),
          child: RichText(
            text: TextSpan(
              text: "Salary : " + salary,
              style: TextStyle(
                color: Colors.black,
                fontStyle: FontStyle.italic,
                fontSize: 15.0,
              ),
            ),
          ),
        )
    );
    return detailsList;
  }
}
