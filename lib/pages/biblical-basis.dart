import 'package:flutter/material.dart';
import 'package:uesi_cg_app/markdown/flutter_markdown.dart';
import 'package:meta/meta.dart';
import 'package:uesi_cg_app/constants.dart';

import 'package:uesi_cg_app/drawer.dart';

class BiblicalBasis extends StatefulWidget {
  final DrawerItem drawerItem;

  BiblicalBasis({Key key, @required this.drawerItem}) : super(key: key);

  @override
  BiblicalBasisState createState() {
    return new BiblicalBasisState();
  }
}

class BiblicalBasisState extends State<BiblicalBasis> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(
          widget.drawerItem.title,
        ),
      ),
      drawer: new NavigationDrawerDemo(),
      body: getMarkDown(),
    );
  }

  Markdown getMarkDown()
  {
    return Markdown(data: Constants.biblicalBasisText);
  }

}
