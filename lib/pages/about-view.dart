import 'package:flutter/material.dart';
import 'package:uesi_cg_app/markdown/flutter_markdown.dart';
import 'package:meta/meta.dart';
import 'package:uesi_cg_app/constants.dart';

import 'package:uesi_cg_app/drawer.dart';

class AboutView extends StatefulWidget {
  final String viewTitle;
  final String viewContent;

  AboutView({Key key, this.viewContent, this.viewTitle}) : super(key: key);

  @override
  AboutViewState createState() {
    return new AboutViewState();
  }
}

class AboutViewState extends State<AboutView> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(
          widget.viewTitle,
        ),
      ),
      drawer: new NavigationDrawerDemo(),
      body: getAboutPage(),
    );
  }

  ListView getAboutPage() {

    List<Widget> listView = new List<Widget>();

    listView.add(
        new Container(
          padding: EdgeInsets.all(20.0),
          child: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
                text: "NEGF CG Cell Members",
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold,
                )
            ),
          ),
        )
    );

    listView.add(
      new Container(
        padding: EdgeInsets.only(
          left: 20.0,
          top: 0.0,
          right: 20.0,
        ),
        child: RichText(
          textAlign: TextAlign.justify,
          text: TextSpan(
            text: "Dr. David Sudhakar - Chairperson"
                + "\nMr. Asir Thiyagarajan - Staff Incharge"
                + "\nDr. Anitha - Member"
                + "\nMr. Paramjothi - Member"
                + "\nMr. Peter Christopher - Member"
                + "\nMr. Daniel Gunjal - Member"
                + "\n\nMr. Selwyn Samraj - NEGF Chairman"
                + "\nMr. Theophilus - NEGF Secretary"
                + "\nMr. Sundersingh - Graduate Ministry Secretary",
            style: TextStyle(
              height: 1.5,
              color: Colors.black,
              fontSize: 15.0,
            ),
          ),
        ),
      ),
    );

    listView.add(
        new Container(
          padding: EdgeInsets.only(
            top: 20.0,
            left: 20.0,
            bottom: 10.0,
          ),
          child: RichText(
            text: TextSpan(
                text: "Contact",
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 18.0,
                  fontWeight: FontWeight.bold,
                ),
            ),
          ),
        )
    );

    listView.add(
      new Container(
        padding: EdgeInsets.only(
          left: 20.0,
          right: 20.0,
        ),
        child: RichText(
          textAlign: TextAlign.justify,
          text: TextSpan(
            text: "Dr. David Sudhakar"
                + "\n98949 78734 | davidsudhakar@yahoo.co.in"
                + "\n\nMr. Peter Christoper"
                + "\n90922 24230 | kristophany@yahoo.co.in",
            style: TextStyle(
              height: 1.5,
              color: Colors.black,
              fontSize: 15.0,
            ),
          ),
        ),
      ),
    );

    return new ListView(children: listView);
  }

}
