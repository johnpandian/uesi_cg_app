import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:uesi_cg_app/markdown/flutter_markdown.dart';
import 'package:meta/meta.dart';
import 'package:uesi_cg_app/constants.dart';

import 'package:uesi_cg_app/drawer.dart';
import 'package:uesi_cg_app/pages/courses-list.dart';
import 'package:uesi_cg_app/pages/exams-list.dart';
import 'package:uesi_cg_app/pages/streams-list.dart';

class ExamTypes extends StatefulWidget {
  final DrawerItem drawerItem;

  ExamTypes({Key key, @required this.drawerItem}) : super(key: key);

  @override
  ExamTypesState createState() {
    return new ExamTypesState();
  }
}

class ExamTypesState extends State<ExamTypes> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(
          widget.drawerItem.title,
        ),
      ),
      drawer: new NavigationDrawerDemo(),
      body:  getExamTypesList(),
    );
  }

  onSelectItem(String examType)
  {
    Navigator.push(
      context,
      new MaterialPageRoute(
        builder: (context) => new ExamsList(
          examType: examType,
        ),
      ),
    );
  }

  Widget getExamTypesList()
  {
    List<Widget> tiles = new List<Widget>();
    Map<String, dynamic> examsListMap = Constants.getExamsListMap();
    for(String type in Constants.getCategoryList())
    {
      if(!examsListMap.containsKey(type)){
        continue;
      }
      List<dynamic> examsList = examsListMap[type];
      tiles.add(
        Card(
          child: ListTile(
            title: Text(type),
            subtitle: Text(examsList.length.toString() + " exams"),
            leading: new Image(
              image: new AssetImage('assets/icons/' + Constants.getIconName(type)),
              height: 50.0,
              width: 50.0,
            ),
            onTap: () => onSelectItem(type),
          ),
        )
      );
    }
    return new ListView(children: tiles);
  }
}
