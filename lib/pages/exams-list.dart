import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:uesi_cg_app/course-data.dart';
import 'package:uesi_cg_app/markdown/flutter_markdown.dart';
import 'package:meta/meta.dart';
import 'package:uesi_cg_app/constants.dart';

import 'package:uesi_cg_app/drawer.dart';
import 'package:uesi_cg_app/pages/course-view.dart';
import 'package:uesi_cg_app/pages/exam-view.dart';

class ExamsList extends StatefulWidget
{
  final String examType;

  ExamsList({Key key, @required this.examType}) : super(key: key);

  @override
  ExamsListState createState() {
    return new ExamsListState();
  }
}

class ExamsListState extends State<ExamsList> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(
          widget.examType + " Exams",
        ),
      ),
      drawer: new NavigationDrawerDemo(),
      body:  getStreamsListView(),
    );
  }

  onSelectItem(String examName, Map<String,dynamic> exam)
  {
    Navigator.push(
      context,
      new MaterialPageRoute(
        builder: (context) => new ExamView(
            exam : exam,
            examName : examName
        ),
      ),
    );
  }

  Widget getStreamsListView()
  {
    List<Widget> tiles = new List<Widget>();
    Map<String, dynamic> examsListMap = Constants.getExamsListMap();
    List<dynamic> examsList = examsListMap[widget.examType];
    int itr = 1;
    for(Map<String,dynamic> exam in examsList)
    {
        tiles.add(
          Card(
            child: ListTile(
              title: Text(itr.toString() + ". " + exam['name']),
              //leading: new Text(itr.toString()),
              subtitle: new Text(
                exam['fname'],
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
              onTap: () => onSelectItem(exam['name'],exam),
            ),
          )
        );
        itr++;
    }
    return new ListView(children: tiles);
  }
}
