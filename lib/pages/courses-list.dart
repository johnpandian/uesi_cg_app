import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:uesi_cg_app/course-data.dart';
import 'package:uesi_cg_app/markdown/flutter_markdown.dart';
import 'package:meta/meta.dart';
import 'package:uesi_cg_app/constants.dart';

import 'package:uesi_cg_app/drawer.dart';
import 'package:uesi_cg_app/pages/course-view.dart';

class CoursesList extends StatefulWidget
{
  final String courseType;
  final String streamName;

  CoursesList({Key key, @required this.streamName, this.courseType}) : super(key: key);

  @override
  CoursesListState createState() {
    return new CoursesListState();
  }
}

class CoursesListState extends State<CoursesList> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(
          widget.streamName + " Courses",
        ),
      ),
      drawer: new NavigationDrawerDemo(),
      body:  getStreamsListView(),
    );
  }

  onSelectItem(String courseName, Map<String,dynamic> course)
  {
    Navigator.push(
      context,
      new MaterialPageRoute(
        builder: (context) => new CourseView(
          course : course,
          courseTitle : courseName
        ),
      ),
    );
  }

  Widget getStreamsListView()
  {
    List<Widget> tiles = new List<Widget>();
    Map<String, dynamic> ugCounsesJson = CourseData.getCoursesList(widget.courseType);
    List<dynamic> coursesList = ugCounsesJson[widget.streamName];
    int itr = 1;
    for(Map<String,dynamic> course in coursesList)
    {
        tiles.add(
          Card(
            child: ListTile(
              title: Text(itr.toString() + ". " + course['name']),
              //leading: new Text(itr.toString()),
              subtitle: new Text("Duration : " + course['duration']),
              onTap: () => onSelectItem(course['name'],course),
            ),
          )
        );
        itr++;
    }
    return new ListView(children: tiles);
  }
}
