import 'package:flutter/material.dart';
import 'package:uesi_cg_app/markdown/flutter_markdown.dart';
import 'package:meta/meta.dart';
import 'package:uesi_cg_app/constants.dart';

import 'package:uesi_cg_app/drawer.dart';

class StudyTechniques extends StatefulWidget {
  final DrawerItem drawerItem;

  StudyTechniques({Key key, @required this.drawerItem}) : super(key: key);

  @override
  StudyTechniquesState createState() {
    return new StudyTechniquesState();
  }
}

class StudyTechniquesState extends State<StudyTechniques> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(
          widget.drawerItem.title,
        ),
      ),
      drawer: new NavigationDrawerDemo(),
      body: Markdown(data : Constants.studyTechniquesText),
    );
  }
}
