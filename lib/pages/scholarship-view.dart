import 'dart:convert';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:uesi_cg_app/markdown/flutter_markdown.dart';
import 'package:meta/meta.dart';
import 'package:uesi_cg_app/constants.dart';

import 'package:uesi_cg_app/drawer.dart';
import 'package:uesi_cg_app/pages/courses-list.dart';
import 'package:url_launcher/url_launcher.dart';


class ScholarshipView extends StatefulWidget {

  final String scholarshipName;

  ScholarshipView({Key key, @required this.scholarshipName}) : super(key: key);

  @override
  ScholarshipViewState createState() {
    return new ScholarshipViewState();
  }
}

class ScholarshipViewState extends State<ScholarshipView> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(
          widget.scholarshipName,
        ),
      ),
      drawer: new NavigationDrawerDemo(),
      body:  getListView(),
    );
  }

  Widget getListView()
  {
    List<Widget> listView = new List<Widget>();

    List<dynamic> scholarshipsList = Constants.getScholarshipList();
    Map<String,dynamic> scholarship = null;
    for(Map<String,dynamic> sc in scholarshipsList) {
      String name = sc['name'];
      if(name.toString() == widget.scholarshipName){
        scholarship = sc;
        break;
      }
    }

    String name = null;
    String address = null;
    String contact = null;
    String information = null;
    if(scholarship != null){
      List<dynamic> websites = null;

      name = scholarship['name'];
      listView.add(
          new Container(
            padding: EdgeInsets.all(20.0),
            child: RichText(text: TextSpan(
                text: name,
                style: TextStyle(
                  fontSize: 20.0,
                  color: Colors.black,
                )
            ),
            ),
          )
      );

      if(scholarship.containsKey('Information')){
        information = scholarship['Information'];
      }

      if(scholarship.containsKey('Address')){
        address = scholarship['Address'];
      }
      if(scholarship.containsKey('Contact')){
        contact = scholarship['Contact'];
      }
      if(scholarship.containsKey('Website')){
        websites = scholarship['Website'];
      }

      if(information != null) {
        listView.add(
          new Container(
            padding: EdgeInsets.only(
              left: 20.0,
              right: 20.0,
              bottom: 10.0
            ),
            child: new RichText(
              text: TextSpan(
                text: "Information",
                style: TextStyle(
                  fontSize: 18.0,
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                )
              )
            ),
          )
        );

        listView.add(
            new Container(
              padding: EdgeInsets.only(
                left: 20.0,
                right: 20.0,
              ),
              child: new RichText(
                textAlign: TextAlign.justify,
                text: TextSpan(
                    text: information,
                    style: TextStyle(
                      fontSize: 16.0,
                      color: Colors.black,
                    )
                )
              ),
            )
        );
      }

      if(contact != null) {
        listView.add(
            new Container(
              padding: EdgeInsets.only(
                  left: 20.0,
                  right: 20.0,
                  top: 20.0,
                  bottom: 10.0
              ),
              child: new RichText(
                  text: TextSpan(
                      text: "Contact",
                      style: TextStyle(
                        fontSize: 18.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                      )
                  )
              ),
            )
        );

        listView.add(
            new Container(
              padding: EdgeInsets.only(
                left: 20.0,
                right: 20.0,
              ),
              child: new RichText(
                  text: TextSpan(
                      text: contact,
                      style: TextStyle(
                        fontSize: 16.0,
                        color: Colors.black,
                      )
                  )
              ),
            )
        );
      }

      if(address != null) {
        listView.add(
            new Container(
              padding: EdgeInsets.only(
                  left: 20.0,
                  right: 20.0,
                  top: 20.0,
                  bottom: 10.0
              ),
              child: new RichText(
                  text: TextSpan(
                      text: "Address",
                      style: TextStyle(
                        fontSize: 18.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                      )
                  )
              ),
            )
        );

        listView.add(
            new Container(
              padding: EdgeInsets.only(
                left: 20.0,
                right: 20.0,
              ),
              child: new RichText(
                  text: TextSpan(
                      text: address,
                      style: TextStyle(
                        fontSize: 16.0,
                        color: Colors.black,
                      )
                  )
              ),
            )
        );
      }

      if(websites != null) {
        listView.add(
            new Container(
              padding: EdgeInsets.only(
                  left: 20.0,
                  right: 20.0,
                  top: 20.0,
                  bottom: 10.0
              ),
              child: new RichText(
                  text: TextSpan(
                      text: "Website",
                      style: TextStyle(
                        fontSize: 18.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                      )
                  )
              ),
            )
        );

        for(String website in websites) {

          listView.add(
              new Container(
                padding: EdgeInsets.only(
                  left: 20.0,
                  right: 20.0,
                ),
                child: new RichText(
                    text: TextSpan(
                      text: website,
                      style: TextStyle(
                        fontSize: 16.0,
                        color: Colors.blue,
                      ),
                      recognizer: new TapGestureRecognizer()
                        ..onTap = () { launch(website);
                      }
                    )
                ),
              )
          );
        }
      }
    }

    listView.add(
      new Container(
        padding: EdgeInsets.all(20.0),
      )
    );

    return new ListView(children: listView);
  }

}
