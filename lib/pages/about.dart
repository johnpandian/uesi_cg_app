import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:uesi_cg_app/constants.dart';

import 'package:uesi_cg_app/drawer.dart';
import 'package:uesi_cg_app/markdown/flutter_markdown.dart';
import 'package:uesi_cg_app/pages/about-view.dart';

class AboutPage extends StatefulWidget {
  final DrawerItem drawerItem;

  AboutPage({Key key, @required this.drawerItem}) : super(key: key);

  @override
  AboutPageState createState() {
    return new AboutPageState();
  }
}

class AboutPageState extends State<AboutPage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(
          widget.drawerItem.title,
        ),
      ),
      drawer: new NavigationDrawerDemo(),
      body: getAboutPage()
      //Markdown(data: Constants.aboutText),
    );
  }

  ListView getAboutPage() {

    List<Widget> listView = new List<Widget>();

    listView.add(
      new Container(
        padding: EdgeInsets.all(20.0),
        child: RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
            text: "NEGF Career Guidance Cell",
            style: TextStyle(
              color: Colors.black,
              fontSize: 20.0,
              fontWeight: FontWeight.bold,
            )
          ),
        ),
      )
    );

    listView.add(
      new Container(
        padding: EdgeInsets.only(
          left: 20.0,
          top: 0.0,
          right: 20.0,
        ),
        child: RichText(
          textAlign: TextAlign.justify,
          text: TextSpan(
            text: "1. Giving Career Guidance to the UESI Constituency through State CG Cells."
            + "\n2. Providing resource persons, materials and modules on Career Guidance."
            + "\n3. Encouraging and ensuring the UESI Graduates' presence and subsequent quality Christian witness in every strata of our society."
            + "\n4. Updating CG App with new  articles on  facing  interviews,  information and tips on appearing  in competitive examinations, providing sources of scholarship and employment opportunities."
            + "\n5. Conducting Training programs for State CG Chairpersons"
            + "\n6. Motivating State CG Cells to conduct state level programs exclusively for the unemployed graduates and  give them moral support."
            + "\n7. Collecting unemployed graduates data and mapping them with suitable employment.",
            style: TextStyle(
              height: 1.5,
              color: Colors.black,
              fontSize: 15.0,
            ),
          ),
        ),
      ),
    );

    listView.add(
      Container(
        child: new Container(
          padding: EdgeInsets.only(top: 10.0),
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.center,
             children: <Widget>[
               new FlatButton(
                 padding: EdgeInsets.all(10.0),
                 color: Colors.blue,
                 textColor: Colors.white,
                 child: Text("CG Cell Members"),
                 onPressed: () => onSelectItem(1),
               ),
               /*new Container(
                 padding: EdgeInsets.all(10.0),
                 child: Text(""),
               ),
               new FlatButton(
                 padding: EdgeInsets.all(10.0),
                 color: Colors.blue,
                 textColor: Colors.white,
                 child: Text("App Dev Team"),
                 onPressed: () => onSelectItem(2),
               ),*/
             ],
          ),
        ),
      ),
    );

    return new ListView(children: listView);
  }

  onSelectItem(int option)
  {
    Navigator.push(
      context,
      new MaterialPageRoute(
        builder: (context) => new AboutView(
          viewTitle: option == 1 ? "NEGF CG Cell Members" : "CG App Dev Team",
          viewContent: Constants.cgCellMembers,
        ),
      ),
    );
  }

}


//void main() {
//  runApp(new MaterialApp(
//      title: "Markdown Demo",
//      home: new Scaffold(
//          appBar: new AppBar(title: const Text('Markdown Demo')),
//          body: const Markdown(data: _markdownData)
//      )
//  ));
//}
