import 'package:flutter/material.dart';
import 'package:uesi_cg_app/markdown/flutter_markdown.dart';
import 'package:meta/meta.dart';
import 'package:uesi_cg_app/constants.dart';

import 'package:uesi_cg_app/drawer.dart';

class ArticleView extends StatefulWidget {
  final int articleNo;
  final String articleTitle;

  ArticleView({Key key, this.articleNo, this.articleTitle}) : super(key: key);

  @override
  ArticleViewState createState() {
    return new ArticleViewState();
  }
}

class ArticleViewState extends State<ArticleView> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(
          widget.articleTitle,
        ),
      ),
      drawer: new NavigationDrawerDemo(),
      body: new Markdown(data: Constants.getArticleContent(widget.articleNo)),
    );
  }
}
