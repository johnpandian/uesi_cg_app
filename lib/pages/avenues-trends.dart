import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:uesi_cg_app/course-data.dart';
import 'package:uesi_cg_app/markdown/flutter_markdown.dart';
import 'package:meta/meta.dart';
import 'package:uesi_cg_app/constants.dart';

import 'package:uesi_cg_app/drawer.dart';

class AvenuesTrends extends StatefulWidget {
  final DrawerItem drawerItem;

  AvenuesTrends({Key key, @required this.drawerItem}) : super(key: key);

  @override
  AvenuesTrendsState createState() {
    return new AvenuesTrendsState();
  }
}

class AvenuesTrendsState extends State<AvenuesTrends> {
  @override
  Widget build(BuildContext context) {
    Container body = new Container(
      child: new Column(
        children: <Widget>[
          new Text("Hello John"),
          Markdown(data: Constants.avenuesTrendsText),
        ],
      ),
    );
    //Markdown(data: Constants.avenuesTrendsText),
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(
          widget.drawerItem.title,
        ),
      ),
      drawer: new NavigationDrawerDemo(),
      body:  getListView(),
    );
  }

  Widget getListView()
  {
    List<Widget> tiles = new List<Widget>();
    tiles.add(
      new Container(
        padding: EdgeInsets.all(10.0),
        child: new RichText(
            text: TextSpan(
              text: "Streams",
              style: TextStyle(
                color: Colors.black,
                fontSize: 20.0,
              ),
            ),

        ),
      )
    );
    Map<String, dynamic> ugCounsesJson = CourseData.getCoursesList('Computer Applications');
    List<dynamic> list = ugCounsesJson['Computer Applications'];
    for(String key in ugCounsesJson.keys)
    {
      List<dynamic> courses = ugCounsesJson[key];
        tiles.add(
          Card(
            child: ListTile(
              title: Text(key),
              subtitle: new Text(courses.length.toString() + " Courses"),
            ),
          )
        );
    }
    /*for(Map<String,dynamic> obj in list)
    {
      tiles.add(
        Card(
          child: ListTile(
            title: Text(obj['name']),
            subtitle: Text(obj['duration']),
          ),
        )
      );
    }*/
    return new ListView(children: tiles);
  }

  @override
  bool get mounted {

  }
}
