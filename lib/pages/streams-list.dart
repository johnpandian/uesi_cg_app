import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:uesi_cg_app/course-data.dart';
import 'package:uesi_cg_app/markdown/flutter_markdown.dart';
import 'package:meta/meta.dart';
import 'package:uesi_cg_app/constants.dart';

import 'package:uesi_cg_app/drawer.dart';
import 'package:uesi_cg_app/pages/courses-list.dart';

class StreamsList extends StatefulWidget {

  final String courseType;

  StreamsList({Key key, @required this.courseType}) : super(key: key);

  @override
  StreamsListState createState() {
    return new StreamsListState();
  }
}

class StreamsListState extends State<StreamsList> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(
          widget.courseType,
        ),
      ),
      drawer: new NavigationDrawerDemo(),
      body:  getStreamsListView(),
    );
  }

  onSelectItem(String streamName)
  {
    Navigator.push(
      context,
      new MaterialPageRoute(
        builder: (context) => new CoursesList(
          courseType: widget.courseType,
          streamName: streamName,
        ),
      ),
    );
  }

  Widget getStreamsListView()
  {
    List<Widget> tiles = new List<Widget>();
    Map<String, dynamic> coursesJson = CourseData.getCoursesList(widget.courseType);
    for(String key in coursesJson.keys)
    {
      //print("Icon : " + Constants.getIconName(key));
      List<dynamic> courses = coursesJson[key];
        tiles.add(
          Card(
            child: ListTile(
              title: Text(key),
              subtitle: new Text(courses.length.toString() + " Courses"),
              leading: new Image(
                height: 35.0,
                width: 35.0,
                image: new AssetImage('assets/icons/' + Constants.getIconName(key)),
              ),
              onTap: () => onSelectItem(key),
            ),
          )
        );
    }
    return new ListView(children: tiles);
  }
}
