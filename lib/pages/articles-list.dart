import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:uesi_cg_app/markdown/flutter_markdown.dart';
import 'package:meta/meta.dart';
import 'package:uesi_cg_app/constants.dart';

import 'package:uesi_cg_app/drawer.dart';
import 'package:uesi_cg_app/pages/article-view.dart';
import 'package:uesi_cg_app/pages/courses-list.dart';
import 'package:uesi_cg_app/pages/scholarship-view.dart';
import 'package:uesi_cg_app/pages/streams-list.dart';

class ArticlesList extends StatefulWidget {
  final DrawerItem drawerItem;

  ArticlesList({Key key, @required this.drawerItem}) : super(key: key);

  @override
  ArticlesListState createState() {
    return new ArticlesListState();
  }
}

class ArticlesListState extends State<ArticlesList> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(
          widget.drawerItem.title,
        ),
      ),
      drawer: new NavigationDrawerDemo(),
      body:  getListView(),
    );
  }

  onSelectItem(int no, String title)
  {
    Navigator.push(
      context,
      new MaterialPageRoute(
        builder: (context) => new ArticleView(
          articleNo: no,
          articleTitle: title,
        ),
      ),
    );
  }

  Widget getListView()
  {
    List<Widget> tiles = new List<Widget>();

    List<String> articleTitles = Constants.getArticlesNames();

    //int i=0;
    for(int i=0;i<articleTitles.length;i++){
      String title = articleTitles[i];
      tiles.add(
        Card(
          child: ListTile(
            isThreeLine: true,
            title: Text(title),
            subtitle: Text(
              Constants.getArticleDescription(i),
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
            ),
            leading: new Image(
              image: new AssetImage('assets/icons/' + Constants.getIconName(title)),
              height: 60.0,
              width: 60.0,
            ),
            onTap: () => onSelectItem(i, title),
          ),
        )
      );
    }

    return new ListView(children: tiles);
  }
}
