import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:uesi_cg_app/markdown/flutter_markdown.dart';
import 'package:meta/meta.dart';
import 'package:uesi_cg_app/constants.dart';

import 'package:uesi_cg_app/drawer.dart';
import 'package:uesi_cg_app/pages/courses-list.dart';
import 'package:uesi_cg_app/pages/scholarship-view.dart';
import 'package:uesi_cg_app/pages/streams-list.dart';

class ScholarshipsList extends StatefulWidget {
  final DrawerItem drawerItem;

  ScholarshipsList({Key key, @required this.drawerItem}) : super(key: key);

  @override
  ScholarshipsListState createState() {
    return new ScholarshipsListState();
  }
}

class ScholarshipsListState extends State<ScholarshipsList> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(
          widget.drawerItem.title,
        ),
      ),
      drawer: new NavigationDrawerDemo(),
      body:  getListView(),
    );
  }

  onSelectItem(String scholarshipName)
  {
    Navigator.push(
      context,
      new MaterialPageRoute(
        builder: (context) => new ScholarshipView(
          scholarshipName : scholarshipName,
        ),
      ),
    );
  }

  Widget getListView()
  {
    List<Widget> tiles = new List<Widget>();

    List<dynamic> scholarshipsList = Constants.getScholarshipList();
    for(Map<String,dynamic> scholarship in scholarshipsList)
    {
      String name = scholarship['name'];
      String address = null, contact = null, infomation = null;
      List<dynamic> websites = null;
      if(scholarship.containsKey('Address')){
        address = scholarship['Address'];
      }
      if(scholarship.containsKey('Contact')){
        contact = scholarship['Contact'];
      }
      if(scholarship.containsKey('Infomation')){
        infomation = scholarship['Infomation'];
      }
      if(scholarship.containsKey('Website')){
        websites = scholarship['Website'];
      }

      tiles.add(
        Card(
          child: ListTile(
            title: Text(
              name,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
            ),
            onTap: () => onSelectItem(name),
          ),
        )
      );

    }

    /*String courseTypeCountStr = '{"Certification Courses":"55","Undergraduate Courses":"427","Diploma Courses":"217","Ph.D Courses":"204","After 10th Courses":"25","Postgraduate Courses":"356"}';
    Map<String,dynamic> courseTypeCountMap = jsonDecode(courseTypeCountStr);

    for(String type in Constants.getCourseTypesList())
    {
      tiles.add(
        Card(
          child: ListTile(
            title: Text(type),
            subtitle: Text(courseTypeCountMap[type] + " Courses"),
            leading: new Image(
              image: new AssetImage('assets/icons/' + Constants.getIconName(type)),
              height: 50.0,
              width: 50.0,
            ),
            onTap: () => onSelectItem(type),
          ),
        )
      );
    }*/
    return new ListView(children: tiles);
  }
}
