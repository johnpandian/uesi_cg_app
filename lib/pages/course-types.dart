import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:uesi_cg_app/markdown/flutter_markdown.dart';
import 'package:meta/meta.dart';
import 'package:uesi_cg_app/constants.dart';

import 'package:uesi_cg_app/drawer.dart';
import 'package:uesi_cg_app/pages/courses-list.dart';
import 'package:uesi_cg_app/pages/streams-list.dart';

class CourseTypes extends StatefulWidget {
  final DrawerItem drawerItem;

  CourseTypes({Key key, @required this.drawerItem}) : super(key: key);

  @override
  CourseTypesState createState() {
    return new CourseTypesState();
  }
}

class CourseTypesState extends State<CourseTypes> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(
          widget.drawerItem.title,
        ),
      ),
      drawer: new NavigationDrawerDemo(),
      body:  getStreamsListView(),
    );
  }

  onSelectItem(String courseType)
  {
    Navigator.push(
      context,
      new MaterialPageRoute(
        builder: (context) => new StreamsList(
          courseType: courseType,
        ),
      ),
    );
  }

  Widget getStreamsListView()
  {
    String courseTypeCountStr = '{"Certification Courses":"55","Undergraduate Courses":"427","Diploma Courses":"217","Ph.D Courses":"204","After 10th Courses":"25","Postgraduate Courses":"356"}';
    Map<String,dynamic> courseTypeCountMap = jsonDecode(courseTypeCountStr);

    List<Widget> tiles = new List<Widget>();
    for(String type in Constants.getCourseTypesList())
    {
      tiles.add(
        Card(
          child: ListTile(
            title: Text(type),
            subtitle: Text(courseTypeCountMap[type] + " Courses"),
            leading: new Image(
              image: new AssetImage('assets/icons/' + Constants.getIconName(type)),
              height: 50.0,
              width: 50.0,
            ),
            onTap: () => onSelectItem(type),
          ),
        )
      );
    }
    return new ListView(children: tiles);
  }
}
